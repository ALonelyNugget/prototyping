﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveTest : MonoBehaviour
{
    public GameObject Target;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Target.gameObject.SetActive(true);
        }
    }
}
