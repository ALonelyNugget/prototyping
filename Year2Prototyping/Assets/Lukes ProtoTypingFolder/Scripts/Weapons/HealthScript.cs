﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    public float health = 100;



    void Update()
    {
        if (health <= 0)
        {
            Die();
        }
    }
    public void TakeDamage(float damage)
    {
        health -= damage;
        Debug.Log("Damage Delt " + damage);
    }
    void Die()
    {
        gameObject.SetActive(false);
    }
}
