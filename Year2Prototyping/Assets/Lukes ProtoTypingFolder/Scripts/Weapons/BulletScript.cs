﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float BulletDamage = 10;
    Rigidbody bullet;
    GameObject gun;
    GunScript gunScript;

    Vector3 gunPosition;
    Vector3 bulletSpawnPosition;

    Vector3 bulletFireDirection;

    float bulletSpeed;
    float Countdown;
    void Start()
    {
        gun = GameObject.FindWithTag("PlayerWeapon");
        gunScript = gun.GetComponent<GunScript>();
        bullet = gameObject.GetComponent<Rigidbody>();


        gunPosition = gun.transform.position;
        bulletSpawnPosition = gunScript.SpawnOffset.transform.position;

        bulletFireDirection = bulletSpawnPosition - gunPosition;

        bulletSpeed = gunScript.ReturnBulletSpeed();

        bullet.AddForce(bulletFireDirection * bulletSpeed, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        Countdown += Time.deltaTime;
        if (Countdown >= 4)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            HealthScript EnemyHealth;
            EnemyHealth = collision.gameObject.GetComponent<HealthScript>();
            EnemyHealth.TakeDamage(BulletDamage);
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Envioment")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Player")
        {
            HealthScript EnemyHealth;
            EnemyHealth = collision.gameObject.GetComponent<HealthScript>();
            EnemyHealth.TakeDamage(BulletDamage);
            gameObject.SetActive(false);
        }
        
        
    }
}
