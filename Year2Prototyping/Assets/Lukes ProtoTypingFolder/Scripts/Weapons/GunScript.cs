﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{

    public GameObject Bullet;
    public GameObject SpawnOffset;
    public float BulletSpeed = 50;
    Vector3 SpawnLocation;
    public Quaternion rotation;
    void Start()
    {
        
    }
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            SpawnLocation = SpawnOffset.transform.position;
            GameObject.Instantiate(Bullet, SpawnLocation, rotation);
            
        }
    }
    public float ReturnBulletSpeed()
    {
        return BulletSpeed;
    }
}
