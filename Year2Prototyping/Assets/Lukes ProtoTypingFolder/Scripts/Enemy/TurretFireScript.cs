﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretFireScript : MonoBehaviour
{
    public GameObject Bullet;
    public GameObject SpawnOffset;
    public float BulletSpeed = 50;
    public float FireRate = 2;
    Vector3 SpawnLocation;
    public Quaternion rotation;
    float CountDown;
    void Start()
    {

    }
    void Update()
    {
        CountDown += Time.deltaTime;
        if (CountDown > FireRate)
        {
            SpawnLocation = SpawnOffset.transform.position;
            GameObject.Instantiate(Bullet, SpawnLocation, rotation);
            CountDown = 0;
        }
    }
    public float ReturnBulletSpeed()
    {
        return BulletSpeed;
    }
}
