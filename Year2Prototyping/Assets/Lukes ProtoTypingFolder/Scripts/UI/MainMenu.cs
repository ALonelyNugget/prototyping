﻿
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    public GameObject Menu;
    public GameObject Credits;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OpenCredits()
    {
        Menu.SetActive(false);
        Credits.SetActive(true);
    }
    public void CloseCredits()
    {
        Menu.SetActive(true);
        Credits.SetActive(false);
    }
    public void SceneSwitch(int SceneName)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneName);
        Debug.Log("SceneJump");
    }
    public void Quit()
    {
        Application.Quit();
    }
}
