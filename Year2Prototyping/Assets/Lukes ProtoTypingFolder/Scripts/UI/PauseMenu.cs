﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{
    [HideInInspector]
    public bool IsPaused = false;
    public GameObject PauseMenuObj;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && IsPaused == false)
        {
            PauseGame();
            Debug.Log("Pause");
        }
        else if (Input.GetKeyDown(KeyCode.P) && IsPaused == true)
        {
            Unpause();
            Debug.Log("UnPause");
        }
    }
    void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenuObj.SetActive(true);
        IsPaused = true;
    }
    void Unpause()
    {
        Time.timeScale = 1;
        PauseMenuObj.SetActive(false);
        IsPaused = false;
    }
    public void SceneSwitch(int SceneName)
    {
        
        SceneManager.LoadScene(SceneName);
        Debug.Log("SceneJump");
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void Resume()
    {
        Unpause();
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void BackToMenu()
    {
        SceneSwitch(0);
    }
}
