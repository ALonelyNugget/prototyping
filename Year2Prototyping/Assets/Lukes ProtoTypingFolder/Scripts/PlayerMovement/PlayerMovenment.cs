﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovenment : MonoBehaviour
{
    [Tooltip("Players Base Walk Speed (WASD)")]
    public float playerSpeed = 12;
    [Tooltip("Players Base Jump Height (SPACE)")]
    public float jumpHeight = 20;
    [Tooltip("How many jumps the player gets mid air")]
    public float extraJumps = 1;
    int jumpCount = 0;
    [Tooltip("Players Relative Fall Speed - Earth Gravity(-9.81)")]
    public float gravity = -9.81f;
    [Tooltip("Time it takes the player character to turn")]
    public float turnSpeedTime = 0.2f;
    [HideInInspector]
    public float turnSpeedVelocity;

    //test area
    bool Strafing;
    //test area

    [Tooltip("Connects to Main Camera"), Space]
    public Transform Camera;

    [Header("Programmer Shit - Dont touch :)")]
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    CharacterController cc;
    Vector3 velocity;
    bool isGrounded;

    void Start()
    {
        cc = gameObject.GetComponent<CharacterController>();
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
            Strafing = true;
        else
            Strafing = false;


        // ^^^TestZone^^^
        CheckJumpCounter();
        Walking();

        if (Input.GetKeyDown(KeyCode.Space) && jumpCount < extraJumps)
        {
            Jump();
            jumpCount++;
        }

        velocity.y += gravity * Time.deltaTime;
        cc.Move(velocity * Time.deltaTime);
    }

    public void Walking()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;

        }

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f && Strafing == false)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Camera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSpeedVelocity, turnSpeedTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            cc.Move(moveDir.normalized * playerSpeed * Time.deltaTime);
        }
        //else if (direction.magnitude >= 0.1f && Strafing == true)
        //{
        //    Debug.Log("Strafing");
        //    float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Camera.eulerAngles.y;
        //    
        //    Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        //    cc.Move(moveDir.normalized * playerSpeed * Time.deltaTime);
        //}

    }
    void Jump()
    {
        
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
    }
    void CheckJumpCounter()
    {
        if (isGrounded)
            jumpCount = 0;
    }
    //second jump keeps the player at there first jumps apex for a extened time

}
