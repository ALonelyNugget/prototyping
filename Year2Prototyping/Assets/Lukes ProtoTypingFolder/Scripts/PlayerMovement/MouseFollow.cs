﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour
{

	public float mouseSensitivity = 100f;
	public Transform playerBody;
	float xRotation = 0f;

	//
	public Transform target;
	public float smoothTime = 0.3F;
	private Vector3 velocity = Vector3.zero;
	//
	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}
	void Update()
	{






		if (Input.GetKey(KeyCode.Mouse1))
		{
			float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
			float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

			xRotation -= mouseY;

			xRotation = Mathf.Clamp(xRotation, 0, 20);


			transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
			playerBody.Rotate(Vector3.up * mouseX);

			//
			// Define a target position above and behind the target transform
			//Vector3 targetPosition = target.TransformPoint(new Vector3(0, 2, -10));

			// Smoothly move the camera towards that target position
			//transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
			//

		}

	}
}

