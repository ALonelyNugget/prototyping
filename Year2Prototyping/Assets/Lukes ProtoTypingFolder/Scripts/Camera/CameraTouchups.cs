﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTouchups : MonoBehaviour
{
    public GameObject scriptBoard;
    PauseMenu pauseMenu;
    void Start()
    {
        pauseMenu = scriptBoard.GetComponent<PauseMenu>();
        Cursor.visible = false;
    }
    private void Update()
    {
        if (pauseMenu.IsPaused == true)
        {
            Cursor.visible = true;
        }
        else if (pauseMenu.IsPaused == false)
        {
            Cursor.visible = false;
        }
    }
}
